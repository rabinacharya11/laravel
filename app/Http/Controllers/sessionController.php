<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class sessionController extends Controller
{
   
     public function storeSessionData(Request $request) {
        $request->session()->put('x',2);
       return view('sessionStore');

     }
     public function deleteSessionData(Request $request) {
        $request->session()->forget('x');
       return view('sessionDel');
     }
}
